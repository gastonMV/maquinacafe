package maquinacafe;

public class CafeNormal extends Cafe{
     private int precioCafeNormal =  90;
    
    public CafeNormal(int tipoBebida,int cantidadAzucar,int cantidadLeche, double iva){
        super(tipoBebida,cantidadAzucar,cantidadLeche,iva);
    }
    
     public double getPrecio(){
        return precioCafeNormal;
    }
    
    @Override
    public String getDescripcion() {
        return " Cafe normal" ;
    }

    @Override
    public double getPrecioBase() {
         return getPrecio();  
    }
}
