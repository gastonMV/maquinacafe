package maquinacafe;
import java.util.Scanner;
public class MaquinaCafe {

    public static void main(String[] args) {
     double iva = 0.21;
        System.out.println("Bienvenido a supermercado Gaston");
        System.out.println("Por favor indiquenos cuantas bebidas va a llevar");
        
        Scanner sc= new Scanner(System.in);
        int cantidad = sc.nextInt();
        
        Bebidas bebidas[] = new Bebidas[cantidad];
        
        String descripcionTodasLasBebidas = "";
        double subTotalBebidas = 0;
        double sumaTotalBebidas = 0;
       
        for(int i = 0; i < bebidas.length;i++){
            
            System.out.println("Ingrese que tipo de bebida desea llevar, 1:Cafe, 2:Te");
                int tipo = sc.nextInt();
                if(tipo == 1){
                System.out.println("Que tipo de cafe va a llevar? 1: Expresso,2:Ristretto, 3:Normal");
                int tipoCafe = sc.nextInt();
                System.out.println("Cantidad de azucar");
                int cantAzucar = sc.nextInt();
                System.out.println("Cantidad de leche");
                int cantLeche = sc.nextInt();
                
                if(tipoCafe == 1){
                    bebidas[i] = new CafeExpresso(tipoCafe,cantAzucar,cantLeche,iva);
                    System.out.println("Descripcion de la bebida: " + bebidas[i].getDescripcionCompleta());
                    System.out.println("Precio base:" + bebidas[i].getPrecioBebidas());
                    System.out.println("Precio total " + (bebidas[i].getPrecioBebidas() + bebidas[i].getPrecioBebidas() * iva )  );      
                }else if(tipoCafe == 2){
                    bebidas[i] = new CafeRistretto(tipoCafe,cantAzucar,cantLeche,iva);
                   System.out.println("Descripcion de la bebida: " + bebidas[i].getDescripcionCompleta());
                   System.out.println("Precio base:" + bebidas[i].getPrecioBebidas());
                   System.out.println("Precio total " + (bebidas[i].getPrecioBebidas() + bebidas[i].getPrecioBebidas() * iva )  );      
                }else if(tipoCafe == 3){
                    bebidas[i] = new CafeNormal(tipoCafe,cantAzucar,cantLeche,iva);
                    System.out.println("Descripcion de la bebida: " + bebidas[i].getDescripcionCompleta());
                    System.out.println("Precio base:" + bebidas[i].getPrecioBebidas());
                    System.out.println("Precio total " + (bebidas[i].getPrecioBebidas() + bebidas[i].getPrecioBebidas() * iva )  );      
                }         
            }else if(tipo == 2){
                System.out.println("Que tipo de Te va a llevar? 1:Rojo, 2:Negro, 3:Verde ");
                int tipoTe = sc.nextInt();
                System.out.println("Cantidad de azucar");
                int cantAzucar = sc.nextInt();
                System.out.println("Cantidad de leche");
                int cantLeche = sc.nextInt();
                
                   if(tipoTe == 1){
                    bebidas[i] = new TeRojo(tipoTe,cantAzucar,cantLeche,iva);
                    System.out.println("Descripcion de la bebida: " + bebidas[i].getDescripcionCompleta());
                    System.out.println("Precio base:" + bebidas[i].getPrecioBebidas());
                    System.out.println("Precio total " + (bebidas[i].getPrecioBebidas() + bebidas[i].getPrecioBebidas() * iva )  );      
                }else if(tipoTe == 2){
                     bebidas[i] = new TeNegro(tipoTe,cantAzucar,cantLeche,iva);
                    System.out.println("Descripcion de la bebida: " + bebidas[i].getDescripcionCompleta());
                     System.out.println("Precio base:" + bebidas[i].getPrecioBebidas());
                     System.out.println("Precio total " + (bebidas[i].getPrecioBebidas() + bebidas[i].getPrecioBebidas() * iva )  );      
                }else if(tipoTe == 3){
                     bebidas[i] = new TeVerde(tipoTe,cantAzucar,cantLeche,iva);
                     System.out.println("Descripcion de la bebida: " + bebidas[i].getDescripcionCompleta());
                     System.out.println("Precio base:" + bebidas[i].getPrecioBebidas());
                     System.out.println("Precio total " + (bebidas[i].getPrecioBebidas() + bebidas[i].getPrecioBebidas() * iva )  );             
                }
            }
           descripcionTodasLasBebidas +=  bebidas[i].getDescripcionCompleta();    
           subTotalBebidas +=  bebidas[i].getPrecioBebidas();
           sumaTotalBebidas += (bebidas[i].getPrecioBebidas() + bebidas[i].getPrecioBebidas() * iva );
        }
        System.out.println("Descripcion de todas sus bebidas: " + descripcionTodasLasBebidas);
        System.out.println("Subtotal: " + subTotalBebidas);
        System.out.println("Total: " + sumaTotalBebidas);
   
    }
    
}
