package maquinacafe;

public class TeVerde extends Te{
    private int precioTeVerde = 75;
    
    public TeVerde(int tipoBebida, int cantidadAzucar,int cantidadLeche,double iva){
        super(tipoBebida,cantidadAzucar,cantidadLeche,iva);
    }
    
     public double getPrecio(){
        return precioTeVerde;
    }
    
   @Override
    public String getDescripcion() {
        return " Te verde" ;
    }

      @Override
    public double getPrecioBase() {
         return getPrecio();  
    }
}
