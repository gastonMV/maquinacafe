package maquinacafe;

public class CafeExpresso extends Cafe{
    private double precioCafeExpresso =  100;
    
    public CafeExpresso(int tipoBebida,int cantidadAzucar,int cantidadLeche, double iva){
        super(tipoBebida,cantidadAzucar,cantidadLeche,iva);
    }
    
     public double getPrecio(){
        return precioCafeExpresso;
    }
    
    @Override
    public String getDescripcion() {
        return " Cafe expresso" ;
    }

    @Override
    public double getPrecioBase() {
         return getPrecio();    
    }
}
