package maquinacafe;

public abstract class Bebidas {
    protected static int tipoBebida;
    protected static  double cantidadAzucar;
    protected static double cantidadLeche;
    protected static double iva;
            
 
    public Bebidas(int tipoBebida,double cantidadAzucar, double cantidadLeche, double iva){
        this.tipoBebida = tipoBebida;
        this.cantidadAzucar = cantidadAzucar;
        this.cantidadLeche = cantidadLeche;
        this.iva = 0.21;
    }
    
    public abstract String getDescripcion();
    public abstract double getPrecioBase();    
    
    public String getDescripcionCompleta(){
     return  getDescripcion() + " con " + cantidadAzucar + " cucharadas de azucar, y con  "
                + cantidadLeche + " cucharadas de leche" ;
    }
    
    public double getPrecioBebidas(){
        return getPrecioBase() + (cantidadAzucar * 5) + (cantidadLeche * 5);    
    }
}
