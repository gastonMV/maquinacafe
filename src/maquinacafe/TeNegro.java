package maquinacafe;

public class TeNegro extends Te{
    private int precioTeNegro  = 70;
    
    public TeNegro(int tipoBebida, int cantidadAzucar,int cantidadLeche,double iva){
        super(tipoBebida,cantidadAzucar,cantidadLeche,iva);
    }
    
     public double getPrecio(){
        return precioTeNegro;
    }
    
    @Override
    public String getDescripcion() {
        return " Te negro" ;
    }

     @Override
    public double getPrecioBase() {
         return getPrecio();  
    }
}
