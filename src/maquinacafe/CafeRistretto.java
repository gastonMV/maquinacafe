package maquinacafe;

public class CafeRistretto extends Cafe{
    private int precioCafeRistretto = 110;
    
    public CafeRistretto(int tipoBebida,int cantidadAzucar,int cantidadLeche, double iva){
        super(tipoBebida,cantidadAzucar,cantidadLeche,iva);
    }
    
    public double getPrecio(){
        return precioCafeRistretto;
    }
    
   @Override
    public String getDescripcion() {
        return " Cafe ristretto" ;
    }

    @Override
    public double getPrecioBase() {
        return getPrecio();    
    }
}
