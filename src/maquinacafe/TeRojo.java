package maquinacafe;

public class TeRojo extends Te{
    private int precioTeRojo = 70;
    
    public TeRojo(int tipoBebida, int cantidadAzucar,int cantidadLeche,double iva){
        super(tipoBebida,cantidadAzucar,cantidadLeche,iva);
    }
    
    public double getPrecio(){
        return precioTeRojo;
    }
    
    @Override
    public String getDescripcion() {
        return " Te Rojo" ;
    }

    @Override
    public double getPrecioBase() {
         return getPrecio();  
}
}
